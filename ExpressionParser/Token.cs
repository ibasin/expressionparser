﻿namespace ExpressionParser
{
    public class Token
    {
        #region EmbeddedTypes
        public enum TypeEnum {Integer, Plus, Minus, Multiply, Divide, OpenParen, CloseParen, EOF}
        #endregion

        #region Constructors
        public Token(TypeEnum type, string value)
        {
            Type = type;
            Value = value;
        }
        #endregion

        #region Overrides
        public override string ToString()
        {
            return Value;
        }
        #endregion

        #region Properties
        public TypeEnum Type { get; }
        public string Value { get; }
        public int IntValue => int.Parse(Value);
        #endregion
    }
}
