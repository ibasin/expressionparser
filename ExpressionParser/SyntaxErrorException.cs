﻿using System;

namespace ExpressionParser
{
    public class SyntaxErrorException : Exception
    {
        #region Constructors
        public SyntaxErrorException(string message, int index) : base(message)
        {
            Index = index;
        }
        #endregion

        #region Properties
        public int Index { get; }
        #endregion
    }
}
