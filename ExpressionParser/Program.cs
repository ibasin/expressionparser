﻿using System;

namespace ExpressionParser
{
    class Program
    {
        static void Main(string[] args)
        {
            while(true)
            {
                try
                {
                    Console.Write("Please enter an expression: ");
                    var expressionString = Console.ReadLine();
                    var parser = new Parser(expressionString);
                    var result = parser.ParseWholeExpression();
                    Console.WriteLine($"{expressionString} = {result}");
                }
                catch (SyntaxErrorException ex)
                {
                    Console.WriteLine($"{ex.Message} at {ex.Index}");
                }
                Console.WriteLine();
            }
        }
    }
}
