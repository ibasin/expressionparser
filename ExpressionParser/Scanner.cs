﻿namespace ExpressionParser
{
    public class Scanner
    {
        #region Constructors
        public Scanner(string expressionString)
        {
            ExpressionString = expressionString;
            Index = 0;
        }
        #endregion

        #region Methods
        public bool IsNext(params Token.TypeEnum[] validTokenTypes)
        {
            var token = PeekNext();
            foreach (var validTokenType in validTokenTypes)
            {
                if (token.Type == validTokenType) return true;
            }
            return false;
        }
        public Token Process(params Token.TypeEnum[] validTokenTypes)
        {
            var token = PeekNext();
            foreach (var validTokenType in validTokenTypes)
            {
                if (token.Type == validTokenType) return GetNext();
            }
            throw new SyntaxErrorException($"{token.Type} when expecting {GetStringList(validTokenTypes)}", Index);
        }
        public string GetStringList(Token.TypeEnum[] tokenTypes)
        {
            var result = "";
            bool first = true;
            foreach (var tokenType in tokenTypes)
            {
                if (first)
                {
                    first = false;
                    result += tokenType.ToString();
                }
                else
                {
                    result += $", or {tokenType.ToString()}";
                }
            }
            return result;
        }
        public Token PeekNext()
        {
            var tmpIndex = Index;
            var token = GetNext();
            Index = tmpIndex;
            return token;
        }
        public Token GetNext()
        {
            SkipWhitespace();
            if (IsEOF()) return new Token(Token.TypeEnum.EOF, "EOF");
            
            var chr = ExpressionString[Index++];
            if (chr == '+') return new Token(Token.TypeEnum.Plus, chr.ToString());
            if (chr == '-') return new Token(Token.TypeEnum.Minus, chr.ToString());
            if (chr == '*') return new Token(Token.TypeEnum.Multiply, chr.ToString());
            if (chr == '/') return new Token(Token.TypeEnum.Divide, chr.ToString());
            if (chr == '(') return new Token(Token.TypeEnum.OpenParen, chr.ToString());
            if (chr == ')') return new Token(Token.TypeEnum.CloseParen, chr.ToString());
            
            Index--;
            if (char.IsDigit(chr)) return new Token(Token.TypeEnum.Integer, ScanInteger());
            else throw new SyntaxErrorException("Unexpected Character", Index);
        }
        #endregion

        #region Private Helper Methods
        protected bool IsEOF()
        {
            return Index >= ExpressionString.Length;
        }
        protected string ScanInteger()
        {
            var intStr = "";
            while(true)
            {
                if (IsEOF()) return intStr;
                var chr = ExpressionString[Index++];
                if (!char.IsDigit(chr)) 
                {
                    Index--;
                    return intStr;
                }
                intStr += chr;
            }
        }
        protected void SkipWhitespace()
        {
            while(true)
            {
                if (IsEOF()) return;
                var chr = ExpressionString[Index++];
                if (!char.IsWhiteSpace(chr))
                {
                    Index--;
                    break;
                }
            }
        }
        #endregion

        #region Properties
        public string ExpressionString { get; }
        public int Index { get; protected set;}
        #endregion
    }
}
