﻿namespace ExpressionParser
{
    public class Parser
    {
        #region Constructors
        public Parser(string expressionString)
        {
            Scanner = new Scanner(expressionString);
        }
        #endregion

        #region Methods
        public int ParseWholeExpression()
        {
            var result = ParseExpression();
            Scanner.Process(Token.TypeEnum.EOF);
            return result;
        }
        public int ParseExpression()
        {
            if (Scanner.IsNext(Token.TypeEnum.Integer, Token.TypeEnum.OpenParen))
            {
                var firstPart = ParseTerm();
                return ParseExpression2(firstPart);
            }
            if (Scanner.IsNext(Token.TypeEnum.Plus, Token.TypeEnum.Minus))
            {
                var addop = Scanner.Process(Token.TypeEnum.Plus, Token.TypeEnum.Minus);
                var firstPart = ParseTerm();
                if (addop.Type == Token.TypeEnum.Minus) firstPart = -firstPart;
                return ParseExpression2(firstPart);
            }
            else
            {
                throw new SyntaxErrorException($"'{Scanner.PeekNext()}' while expecting an integer, or '+', or '-', or '('", Scanner.Index);
            }
        }
        public int ParseExpression2(int firstPart)
        {
            if (Scanner.IsNext(Token.TypeEnum.Plus, Token.TypeEnum.Minus))
            {
                var addop = Scanner.Process(Token.TypeEnum.Plus, Token.TypeEnum.Minus);
                var secondPart = ParseTerm();

                if (addop.Type == Token.TypeEnum.Plus) firstPart += secondPart;
                else firstPart -= secondPart;
                
                return ParseExpression2(firstPart);
            }
            else
            {
                return firstPart;
            }
        }
        public int ParseTerm()
        {
            var firstPart = ParseFactor();
            return ParseTerm2(firstPart);
        }
        public int ParseTerm2(int firstPart)
        {
            if (Scanner.IsNext(Token.TypeEnum.Multiply, Token.TypeEnum.Divide))
            {
                var mulop = Scanner.Process(Token.TypeEnum.Multiply, Token.TypeEnum.Divide);
                var secondPart = ParseFactor();
                
                if (mulop.Type == Token.TypeEnum.Multiply) firstPart *= secondPart;
                else firstPart /= secondPart;

                return ParseTerm2(firstPart);
            }
            else
            {
                return firstPart;
            }
        }
        public int ParseFactor()
        {
            if (Scanner.IsNext(Token.TypeEnum.Integer, Token.TypeEnum.Minus, Token.TypeEnum.Plus))
            {
                return ParseNumConst();
            }
            else if (Scanner.IsNext(Token.TypeEnum.OpenParen))
            {
                Scanner.Process(Token.TypeEnum.OpenParen);
                var result = ParseExpression();
                Scanner.Process(Token.TypeEnum.CloseParen);
                return result;
            }
            else
            {
                throw new SyntaxErrorException($"'{Scanner.PeekNext()}' while expecting an integer, or '+', or '-', or '('", Scanner.Index);
            }
        }
        public int ParseNumConst()
        {
            if (Scanner.IsNext(Token.TypeEnum.Integer))
            {
                return Scanner.Process(Token.TypeEnum.Integer).IntValue;
            }
            else if (Scanner.IsNext(Token.TypeEnum.Plus, Token.TypeEnum.Minus))
            {
                var sign = Scanner.Process(Token.TypeEnum.Plus, Token.TypeEnum.Minus);
                var integer = Scanner.Process(Token.TypeEnum.Integer).IntValue;
                if (sign.Type == Token.TypeEnum.Minus) integer = -integer;
                return integer;
            }
            else
            {
                throw new SyntaxErrorException($"'{Scanner.PeekNext()}' while expecting an integer, or '+', or '-'", Scanner.Index);
            }
        }
        #endregion

        #region Properties
        protected Scanner Scanner { get; }
        #endregion
    }
}
